/* importing all the necessary header file */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

/* memory map size required to be mapped is taken as 4096 bytes (umm..if i am not wrong) */

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

# define BASE_ADDR 0x43C00000

/* main program starts here */

void *virtual_mmap(int physc_addr)
{
	int fd;  /* file descriptor returned on opening of memory if success */
	void *map_base, *virtual_addr;  /* map_base will give the starting address when mmap runs successfully allocating virtual space for us
									virtual_addr will be returned by this function on adding offset */

	off_t dev_base = physc_addr;
	//unsigned long read_something, write_something;

	//if (argc < 2):
	//{
		//fprintf(stderr, "please pass the physical address value\n");
		//exit(1);
	//}

	//dev_base = argv[1]  /* assign the address value*/

	fd = open("/dev/mem", O_RDWR | O_SYNC); 
	if (fd == -1) {
		printf(stderr, "error opening /dev/mem (%d)  [%s]\n", errno, strerror(errno));
		exit(0);
	}
    

    /* memory mapping of the size of page we requested in MAP_SIZE */

	map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, dev_base & ~MAP_MASK);
	if (map_base == (void *) -1) {
		printf(stderr, "error mapping memory to user space (%d) [%s]\n", errno, strerror(errno));
		exit(0);
	}
	printf("memory mapped at address %p.\n", map_base);

	/* assign value to virutal address */
	
	virtual_addr = map_base + (dev_base & MAP_MASK);
	return virtual_addr;

	/*********** dont think so this part will be required in heree **************/

	/* if we are reading on say a word, there are other options as well like byte from this memory*/
	//read_something = *((unsigned long *) virtual_addr);
	//printf("value at address 0x%X (%p): 0x%X\n", dev_base, virtual_addr, read_something);

	/* if we are writing something to this memory and then read back just checking typz sooo.. */
	//*((unsigned long *) virtual_addr) = write_something;
	//read_something = *((unsigned long *) virtual_addr); 

	//printf("data written 0x%X; read now 0x%X\n", write_something, read_something); 

 }


int main(){

	int *dev_base_addr = virtual_mmap(BASE_ADDR);
	int *color_register = dev_base_addr;            /* this is register 0 virtual address */
	int *weight_register = dev_base_addr + 1;        /* this is register 1 virutal address thats 32 bits plus base address of register 0 */
	int *conv_register = dev_base_addr + 2;           /* this is register 2 virutal address thats 32 bits plus address of register 0 */


	/* assigning the values for multiplication */
	*(color_register) = 10;
	*(weight_register) = 20;

	printf("multiplication result: %d\n", *(conv_register));      /* output on convolution register */

	return 0;                                                     /* on success */

}
	





